import psycopg2                 # PostgreSQL adapter
from psycopg2.extras import RealDictCursor # add names of fields to cursor
from random import randrange
from fastapi import Body, FastAPI, HTTPException, Response, status
from time import sleep 
from pydantic import BaseModel  # Schema
from typing import Optional     # Optional fields
from datetime import datetime

# Database Connection
try:
    db_connection = psycopg2.connect(
        host='localhost',
        database= 'Reddit Clone',
        user= 'postgres',
        password = 'API',
        cursor_factory= RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database connection: successful')
except Exception as error:
    print('Database connection: failed')
    print('Error:', error)


class BlogPost(BaseModel):  # Pydantic schema for POST Body validation
    title: str
    content: str
    published: bool = True

    
###### FastAPI instance name ######
app = FastAPI()  



#######################################
#### "Path Operations" or "Routes"  ###
#######################################

# GET / :
@app.get("/")  # decorator
def hello():  # function
    return {"message": "Hello from my first API"}  # response



# GET /posts ***GET ALL BLOGPOSTS***
@app.get("/posts")
def get_posts():
    #Executing the SQL query
    cursor.execute("SELECT * FROM posts")
    # Retrieve all the posts (list)
    database_posts = cursor.fetchall()
    return {"data": database_posts}



# POST /posts  ***CREATE NEW BLOGPOST***
@app.post("/posts", status_code=status.HTTP_201_CREATED)   # POST /posts endpoint
def create_posts(new_post: BlogPost, response: Response):  # Use the pydantic schema class
    cursor.execute("INSERT INTO posts (title, content, published) values (%s ,%s, %s) RETURNING *;", 
        (new_post.title, new_post.content, new_post.published))
    post_dict = cursor.fetchone()
    db_connection.commit() # Save the changes to the Database
    # response.status_code = 201                           # Alternative solution to change the HTTP code
    return {"data": post_dict}   



#GET /posts/{id_param}  ***GET POST WITH ID***
@app.get('/posts/{id_param}')                              # {id_param}  is the path parameter
def get_post(id_param: int, response: Response):           # id_param must be an integer                                       
    cursor.execute("SELECT * FROM posts WHERE id=%s;", (id_param,))  #placeholder %s requires a tuple as parameter
    corresponding_post = cursor.fetchone()
    if not corresponding_post:                             # If No corresponding post found throw an exception to the consumer
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_post}   



#DELETE /posts/{id_param}  ***DELETE POST WITH ID***
@app.delete('/posts/{id_param}')
def delete_post(id_param: int):
    cursor.execute("DELETE FROM posts WHERE id=%s RETURNING *;", (id_param,))
    post_dig = cursor.fetchone()
    db_connection.commit()
    # Exception is not found
    if not post_dig:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return Response(status_code=status.HTTP_204_NO_CONTENT) # Best practice HTTP CODE 204 = not content response 



#PUT /posts/{id_param}  ***REPLACE POST WITH ID***
@app.put('/posts/{id_param}')
def replace_post(id_param: int, updated_post: BlogPost):
    cursor.execute("UPDATE posts SET title=%s, content=%s, published=%s WHERE id=%s RETURNING *;",
        (updated_post.title, updated_post.content, updated_post.published, id_param))
    corresponding_index = cursor.fetchone()
    db_connection.commit()
    if not corresponding_index:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Not corresponding post was found with id:{id_param}"
        )
    return {"data": corresponding_index}
