from fastapi import FastAPI, status, HTTPException
from time import sleep # Sleep make the server waits
import psycopg2 # PostgreSQL adapter
from psycopg2.extras import RealDictCursor # Fields to cursor
from pydantic import BaseModel # Schema
from typing import Optional # For optional quantity only
from datetime import datetime

#Pydantic schema for BlogPost Body validation
class BlogPost(BaseModel):
    title: str
    content: str

#Database Connection
try:
    db_connection = psycopg2.connect(
        host='localhost',
        database= 'onetomany',
        user= 'postgres',
        password = 'API',
        cursor_factory= RealDictCursor
    )
    cursor = db_connection.cursor()
    print('Database connection: successful')
except Exception as error:
    print('Database connection: failed')
    print('Error:', error)

#FastAPI server
app = FastAPI() # API instance name @app.get("/posts")
#Select all blogpost records
@app.get("/posts")
def get_posts():
    #Writing the SQL query
    cursor.execute("SELECT * FROM blogpost")
    #Retrieving all the posts (list/array)
    database_posts = cursor.fetchall()
    return {"data": database_posts}

#Select a blogpost record from its id
@app.get('/posts/{id_uri}')
def get_post(id_uri: int):
    cursor.execute("SELECT * FROM blogpost WHERE id=%i" % id_uri)
    corresponding_post = cursor.fetchone()
    if not corresponding_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": corresponding_post}

#Insert a new blogpost record
@app.post('/posts', status_code=status.HTTP_201_CREATED)
def create_post(post_body: BlogPost):
    cursor.execute("INSERT INTO blogpost (title, content) VALUES(%s, %s) RETURNING *;",
    (post_body.title, post_body.content))
    new_post = cursor.fetchone()
    db_connection.commit(); # Save the changes to the Database
    #return new_post
    return {"data" :new_post}

#Update a blogpost record from its id
@app.put('/posts/{id_uri}')
def update_post(id_uri: int, post_body: BlogPost):
    cursor.execute("UPDATE blogpost SET title=%s, content=%s WHERE id=%s RETURNING *;",
    (post_body.title, post_body.content, str(id_uri)))
    updated_post = cursor.fetchone()
    db_connection.commit()
    if not updated_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": updated_post}


#Delete a blogpost record from its id
@app.delete('/posts/{id_uri}', status_code=status.HTTP_204_NO_CONTENT)
def delete_post(id_uri:int):
    cursor.execute("DELETE FROM blogpost WHERE id=%i RETURNING *;" % id_uri)
    deleting_post = cursor.fetchone()
    db_connection.commit() # Save the changes to the Database
    if not deleting_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
        detail=f"Not corresponding post was found with id:{id_uri}")
    return {"data": deleting_post}
