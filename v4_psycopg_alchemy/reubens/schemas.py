from datetime import datetime
from pydantic import BaseModel, EmailStr

#UserPy
#Pydantic schema for User Body validation
class UserPy(BaseModel):
    email: EmailStr # str is to general (ramdom text)
    password: str
    
# Pydantic schema User Response
class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config:  # Important for Pydantic model/schema transl
        orm_mode = True
        
#BlogPostPy
#Pydantic schema for BlogSpot Body validation

class BlogPostPy(BaseModel):
    title: str
    content: str
    # published: bool = True
    writer_id: int
    
#Pydantic schema BlogPoost Response
class BlogPost_Response(BlogPostPy):
    id: int
    created_at: datetime
    published: bool
    writer_id: User_Response
    class Config:  # important for pydantic model/schemaa translation
        orm_mode = True
