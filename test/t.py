class C:
    def f(self):
        return 8
    def g(self):
        return 4

def test_f():
    c = C()
    assert c.f() == 8

def test_g():
    c = C()
    assert c.g() == 4



