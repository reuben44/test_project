from jose import jwt 
from v4_auth.utilities.jwt_manager import SERVER_KEY, ALGORITHM
from fastapi.testclient import TestClient

#testing the user creation 
import pytest


def test_create_user(client):
    res = client.post("/users", json={"email":"test.user44@domain.lu", "password": "1234"})
    print(res.json())
    assert res.json().get("email") == "test.user44@domain.lu"
    assert res.status_code == 201
    
def test_login_user(create_user, client):
    res = client.post("/auth", data = {"username": create_user["email"],
                                       "password": create_user["password"]})
    assert res.status_code == 202
    assert res.json().get("token_type") == "bearer"
    #verify the token data
    payload = jwt.decode(res.json().get("access_token"), SERVER_KEY, algorithms= [ALGORITHM])
    id = payload.get("user_id")
    assert id == create_user["id"]

def test_me(create_user, authorized_client):
    res = authorized_client.get("/users/me")
    assert res.status_code == 200
    assert res.json().get("id") == create_user ["id"]
    assert res.json().get("email") == create_user ["email"]


@pytest.mark.parametrize("email, password, status_code", 
[("pierre.gillet@cnfpc.lu", None, 422),
(None, "CNFPC!", 422),
("wrongemail@gmail.com", "CNFPC!", 401),
("pierre.gillet@cnfpc.lu", "wrongpassword", 401),
("wrongemail@gmail.com", "wrongpassword", 401)])
def test_incorrect_credentials(create_user, client, email, password, status_code):
    res = client.post("/auth", data={"username": email, "password": password})
    assert res.status_code == status_code

def test_me_incorrect(create_user, client):
    res = client.get('/users/me')
    assert res.status_code == 401
