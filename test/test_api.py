from fastapi.testclient import TestClient
from v4_auth.main import app
import pytest

#client = TestClient(app)


def test_docs(client):
    res = client.get("/docs")
    print(res.status_code)
    assert res.status_code == 200

def test_redocs(client):
    res = client.get("/redoc")
    print(res.status_code)
    assert res.status_code == 200
